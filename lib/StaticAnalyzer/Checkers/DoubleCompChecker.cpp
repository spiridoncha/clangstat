#include "ClangSACheckers.h"
#include "clang/StaticAnalyzer/Core/BugReporter/BugType.h"
#include "clang/StaticAnalyzer/Core/Checker.h"
#include "clang/StaticAnalyzer/Core/CheckerManager.h"
#include "clang/StaticAnalyzer/Core/PathSensitive/CheckerContext.h"

using namespace clang;
using namespace ento;

namespace {
class DoubleCompChecker : public Checker<check::PreStmt<BinaryOperator>> {
  mutable std::unique_ptr<BuiltinBug> BT;

public:
  void checkPreStmt(const BinaryOperator *B, CheckerContext &C) const;
};
}

void DoubleCompChecker::checkPreStmt(const BinaryOperator *B,
                                     CheckerContext &C) const {
  // When doing pointer subtraction, if the two pointers do not point to the
  // same memory chunk, emit a warning.
  if (B->getOpcode() != BO_EQ)
    return;

  ProgramStateRef state = C.getState();
  auto typeLHS = B->getLHS()->getType().getTypePtr();
  auto typeRHS = B->getRHS()->getType().getTypePtr();
  if (not(typeLHS->isScalarType() &&
          typeLHS->getScalarTypeKind() == Type::STK_Floating) ||
      (not typeRHS->isScalarType() &&
       typeRHS->getScalarTypeKind() == Type::STK_Floating))
    return;

  if (ExplodedNode *N = C.generateNonFatalErrorNode()) {
    if (!BT)
      BT.reset(
          new BuiltinBug(this, "Double comparition",
                         "Comparition of two double"));
    auto R = llvm::make_unique<BugReport>(*BT, BT->getDescription(), N);
    R->addRange(B->getSourceRange());
    C.emitReport(std::move(R));
  }
}

void ento::registerDoubleCompChecker(CheckerManager &mgr) {
  mgr.registerChecker<DoubleCompChecker>();
}
