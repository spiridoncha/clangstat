//===--- PathStatistics.cpp - Path-Specific Statictics Collector -- C++ ---===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
//  This file defines the PathStatistics interfaces.
//
//===----------------------------------------------------------------------===//

#include "clang/StaticAnalyzer/Core/BugReporter/PathStatistics.h"
#include "clang/StaticAnalyzer/Core/PathSensitive/ProgramState.h"
#include "clang/StaticAnalyzer/Core/PathSensitive/Environment.h"
#include "clang/AST/Stmt.h"
#include "clang/AST/ASTContext.h"
#include "clang/AST/ASTEnvVisitor.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/ADT/STLExtras.h"
#include "llvm/ADT/DenseMap.h"
#include <vector>
#include <utility>
#include <string>
#include <numeric>

using namespace clang;
using namespace ento;

namespace {

class ProcessBinding : public StoreManager::BindingsHandler {
public:
  virtual bool HandleBinding(StoreManager &SMgr, Store store,
                             const MemRegion *R, SVal val) override;

  PathStatistics::bindingsTy getBindings() const { return bindings; }
  PathStatistics::bindingsTy getNonExprBindings() const {
    return nonExprBindings;
  }
  PathStatistics::namesTy getNames() const { return names; }
  PathStatistics::namesTy getNonExprNames() const { return nonExprNames; }

private:
  PathStatistics::bindingsTy::mapped_type getProperties(const MemRegion *);
  PathStatistics::bindingsTy bindings;
  PathStatistics::bindingsTy nonExprBindings;
  PathStatistics::namesTy names;
  PathStatistics::namesTy nonExprNames;
};

template <class T>
bool isThis(const MemRegion *R) {
  return isa<T>(R->getMemorySpace()) ? true : false;
}

PathStatistics::bindingsTy::mapped_type
ProcessBinding::getProperties(const MemRegion *R) {
  PathStatistics::bindingsTy::mapped_type properties;
  properties.insert({"global", isThis<GlobalsSpaceRegion>(R)});
  properties.insert({"heap", isThis<HeapSpaceRegion>(R)});
  properties.insert({"stack_arguments", isThis<StackArgumentsSpaceRegion>(R)});
  properties.insert({"stack_local", isThis<StackLocalsSpaceRegion>(R)});
  properties.insert({"unknown", isThis<UnknownSpaceRegion>(R)});
  return properties;
}

bool ProcessBinding::HandleBinding(StoreManager &SMgr, Store store,
                                   const MemRegion *R, SVal val) {
  llvm::FoldingSetNodeID ID;
  auto *expr = val.getAsSymbolicExpression();
  if (!expr) {
    llvm::FoldingSetNodeID ID;
    R->Profile(ID);
    auto uniqueHash = ID.ComputeHash();
    nonExprBindings.insert({uniqueHash, getProperties(R)});
    nonExprNames.insert({uniqueHash, R->getString()});
    return true;
  }
  expr->Profile(ID);
  auto uniqueHash = ID.ComputeHash();
  names.insert({uniqueHash, R->getString()});
  bindings.insert({uniqueHash, getProperties(R)});
  return true;
}

std::string getNameWithoutQuotes(const std::string &s) {
  if (s.empty()) {
    return s;
  }
  if (s[0] != '\'') {
    return s;
  }
  return s.substr(1, s.length() - 2);
}

} // end anonymous namespace

namespace clang {

namespace ento {

class PathStatistics::PathStatisticsImpl {
private:
  std::string stmtClassName;
  bindingsTy bindings;
  bindingsTy nonExprBindings;
  namesTy names;
  namesTy nonExprNames;
  constraintsTy constraints;
  environmentTy environment;
  conditionTy condition;

public:
  PathStatisticsImpl() = default;
  PathStatisticsImpl(const PathStatisticsImpl &) = delete;
  PathStatisticsImpl(PathStatisticsImpl &&) = delete;
  PathStatisticsImpl &operator=(const PathStatisticsImpl &) = delete;
  PathStatisticsImpl &operator=(PathStatisticsImpl &&) = delete;
  ~PathStatisticsImpl() = default;

  void addPartOfPathStatistics(const Stmt *);
  void addPartOfPathStatistics(const std::string &, const std::string &,
                               const std::string &);
  void addPartOfPathStatistics(ASTContext &, const Stmt *,
                               const ASTEnvVisitor &);
  void addPartOfPathStatistics(StoreManager &, const ProgramState &);

  const std::string &getStmtClassName() const { return stmtClassName; }

  void setStmtClassName(const std::string &stmtClassName) {
    this->stmtClassName = stmtClassName;
  }

  const bindingsTy &getBindings() const;
  const bindingsTy &getNonExprBindings() const;
  const namesTy &getNames() const;
  const namesTy &getNonExprNames() const;
  const constraintsTy &getConstraints() const;
  const environmentTy &getEnvironment() const;
  const conditionTy &getCondition() const;

  void dump(llvm::raw_ostream &) const;
};

// Must be replaced by method with ASTContext and Stmt as parameters
void PathStatistics::PathStatisticsImpl::addPartOfPathStatistics(
    const Stmt *stmt) {
  if (stmt) {
    stmtClassName = stmt->getStmtClassName();
  }
}

void PathStatistics::PathStatisticsImpl::addPartOfPathStatistics(
    const std::string &var, const std::string &val, const std::string &op) {
  condition =
      Condition(getNameWithoutQuotes(var), getNameWithoutQuotes(val), op);
}

void PathStatistics::PathStatisticsImpl::addPartOfPathStatistics(
    ASTContext &astContext, const Stmt *stmt, const ASTEnvVisitor &visitor) {
  const ASTEnvVisitor::EnvContainer &env = visitor.getEnv(stmt);
  if (env.empty()) {
    environment = "";
    return;
  }
  environment =
      std::accumulate(std::next(env.begin()), env.end(), *(env.begin()),
                      [](const std::string &acc,
                         const ASTEnvVisitor::EnvContainer::value_type &s) {
                        return acc + ' ' + s;
                      });
}

void PathStatistics::PathStatisticsImpl::addPartOfPathStatistics(
    StoreManager &storeMgr, const ProgramState &state) {
  ProcessBinding pB;
  storeMgr.iterBindings(state.getStore(), pB);
  bindings = pB.getBindings();
  names = pB.getNames();
  nonExprBindings = pB.getNonExprBindings();
  nonExprNames = pB.getNonExprNames();
  const ConstraintManager &constraintMgr = state.getConstraintManager();
  constraints = constraintMgr.iterConstraints(state);
  // for (auto i : constraints) {
  //   llvm::errs() << i.first << "\n";
  //   llvm::errs() << i.second << "\n";
  // }
  // llvm::errs() << "\n";
}

const PathStatistics::bindingsTy &
PathStatistics::PathStatisticsImpl::getBindings() const {
  return bindings;
}

const PathStatistics::bindingsTy &
PathStatistics::PathStatisticsImpl::getNonExprBindings() const {
  return nonExprBindings;
}

const PathStatistics::namesTy &
PathStatistics::PathStatisticsImpl::getNames() const {
  return names;
}

const PathStatistics::namesTy &
PathStatistics::PathStatisticsImpl::getNonExprNames() const {
  return nonExprNames;
}

const PathStatistics::constraintsTy &
PathStatistics::PathStatisticsImpl::getConstraints() const {
  return constraints;
}

const PathStatistics::environmentTy &
PathStatistics::PathStatisticsImpl::getEnvironment() const {
  return environment;
}

const PathStatistics::conditionTy &
PathStatistics::PathStatisticsImpl::getCondition() const {
  return condition;
}

// Dump as piece of json
void PathStatistics::PathStatisticsImpl::dump(llvm::raw_ostream &o) const {}

} // end ento namespace

} // end clang namespace

namespace clang {

namespace ento {

PathStatistics::PathStatistics()
    : pImpl(llvm::make_unique<PathStatisticsImpl>()) {}

PathStatistics::~PathStatistics() = default;

// Maybe should be template
void PathStatistics::addPartOfPathStatistics(const Stmt *s) {
  pImpl->addPartOfPathStatistics(s);
}

void PathStatistics::addPartOfPathStatistics(const std::string &var,
                                             const std::string &val,
                                             const std::string &op) {
  pImpl->addPartOfPathStatistics(var, val, op);
}

void PathStatistics::addPartOfPathStatistics(ASTContext &astContext,
                                             const Stmt *s,
                                             const ASTEnvVisitor &visitor) {
  pImpl->addPartOfPathStatistics(astContext, s, visitor);
}

void PathStatistics::addPartOfPathStatistics(StoreManager &storeMgr,
                                             const ProgramState &state) {
  pImpl->addPartOfPathStatistics(storeMgr, state);
}

void PathStatistics::dump() const {
  dump(llvm::errs());
}

void PathStatistics::dump(llvm::raw_ostream &o) const {
  pImpl->dump(o);
}

const std::string &PathStatistics::getStmtClassName() const {
  return pImpl->getStmtClassName();
}

void PathStatistics::setStmtClassName(const std::string &stmtClassName) {
  pImpl->setStmtClassName(stmtClassName);
}

const PathStatistics::bindingsTy &PathStatistics::getBindings() const {
  return pImpl->getBindings();
}

const PathStatistics::bindingsTy &PathStatistics::getNonExprBindings() const {
  return pImpl->getNonExprBindings();
}

const PathStatistics::namesTy &PathStatistics::getNames() const {
  return pImpl->getNames();
}

const PathStatistics::namesTy &PathStatistics::getNonExprNames() const {
  return pImpl->getNonExprNames();
}

const PathStatistics::constraintsTy &PathStatistics::getConstraints() const {
  return pImpl->getConstraints();
}

const PathStatistics::environmentTy &PathStatistics::getEnvironment() const {
  return pImpl->getEnvironment();
}

const PathStatistics::conditionTy &PathStatistics::getCondition() const {
  return pImpl->getCondition();
}

} // end ento namespace

} // end clang namespace
