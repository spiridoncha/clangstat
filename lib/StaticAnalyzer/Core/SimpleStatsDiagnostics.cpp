
//===--- SimpleStatsDiagnostics.cpp - SimpleStats Diagnostics for Paths -----*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
//  This file defines the SimpleStatsDiagnostics object.
//
//===----------------------------------------------------------------------===//

#include "clang/Basic/FileManager.h"
#include "clang/Basic/PlistSupport.h"
#include "clang/Basic/SourceManager.h"
#include "clang/Basic/Version.h"
#include "clang/Lex/Preprocessor.h"
#include "clang/StaticAnalyzer/Core/BugReporter/PathDiagnostic.h"
#include "clang/StaticAnalyzer/Core/BugReporter/PathStatistics.h"
#include "clang/StaticAnalyzer/Core/IssueHash.h"
#include "clang/StaticAnalyzer/Core/PathDiagnosticConsumers.h"
#include "llvm/ADT/SmallVector.h"
#include "llvm/Support/Casting.h"

using namespace clang;
using namespace ento;
using namespace markup;

namespace {
class SimpleStatsDiagnostics : public PathDiagnosticConsumer {
  const std::string OutputFile;
  const LangOptions &LangOpts;
  const bool SupportsCrossFileDiagnostics;

public:
  SimpleStatsDiagnostics(AnalyzerOptions &AnalyzerOpts,
                         const std::string &prefix, const LangOptions &LangOpts,
                         bool supportsMultipleFiles);

  ~SimpleStatsDiagnostics() override {}

  void FlushDiagnosticsImpl(std::vector<const PathDiagnostic *> &Diags,
                            FilesMade *filesMade) override;

  StringRef getName() const override { return "SimpleStatsDiagnostics"; }

  PathGenerationScheme getGenerationScheme() const override {
    return Extensive;
  }
  bool supportsLogicalOpControlFlow() const override { return true; }
  bool supportsCrossFileDiagnostics() const override {
    return SupportsCrossFileDiagnostics;
  }
};

std::string wrapString(const std::string &s) { return "\"" + s + "\""; }

std::string jsonEscape(const std::string &s) {
  std::string res;
  for (auto ch : s) {
    switch (ch) {
    default:
      res.push_back(ch);
      break;
    case '"':
      res.push_back('\\');
      res.push_back('"');
      break;
    case '\\':
      res.push_back('\\');
      res.push_back('\\');
      break;
    case '\n':
    case '\t':
      res.push_back(' ');
      break;
    }
  }
  return res;
}

void EmitKeyValue(raw_ostream &o, const std::string &key,
                  const std::string &value, bool hasNext = true) {
  o << wrapString(key) << ":" << wrapString(jsonEscape(value));
  if (hasNext) {
    o << ",";
  }
}

void EmitKeyBoolValue(raw_ostream &o, const std::string &key, bool value,
                  bool hasNext = true) {
  o << wrapString(key) << ":" << (value ? "true" : "false");
  if (hasNext) {
    o << ",";
  }
}

void EmitKeyValue(raw_ostream &o, const std::string &key, int value,
                  bool hasNext = true) {
  o << wrapString(key) << ":" << value;
  if (hasNext) {
    o << ",";
  }
}

void EmitLocationJson(raw_ostream &o, const SourceManager &SM, SourceLocation L,
                      const FIDMap &FM, bool hasNext = true) {
  if (L.isInvalid()) {
    o << "{}";
    return;
  }

  FullSourceLoc Loc(SM.getExpansionLoc(L), const_cast<SourceManager &>(SM));

  o << "{";
  EmitKeyValue(o, "line", Loc.getExpansionLineNumber());
  EmitKeyValue(o, "col", Loc.getExpansionColumnNumber());
  EmitKeyValue(o, "file", GetFID(FM, SM, Loc), false);
  o << "}";
  if (hasNext) {
    o << ",";
  }
}

void EmitRangeJson(raw_ostream &o, const SourceManager &SM, CharSourceRange R,
                   const FIDMap &FM) {
  if (R.isInvalid())
    return;

  assert(R.isCharRange() && "cannot handle a token range");
  o << "[";
  EmitLocationJson(o, SM, R.getBegin(), FM);
  EmitLocationJson(o, SM, R.getEnd(), FM, false);
  o << "]";
}

void EmitBindings(raw_ostream &o, const std::string &key, const PathStatistics::bindingsTy &bindings) {
  o << wrapString(key);
  o << ":";
  o << "{";
  for (auto i = bindings.begin(), e = bindings.end(); i != e; ++i) {
    bool hasNext = false;
    if (std::next(i) != e) {
      hasNext = true;
    }

    o << wrapString(std::to_string(i->first));
    o << ":";
    o << "{";
    const auto &properties = i->second;
    for (auto ii = properties.begin(), ie = properties.end(); ii != ie; ++ii) {
      bool hasNext = false;
      if (std::next(ii) != ie) {
        hasNext = true;
      }
      EmitKeyBoolValue(o, ii->first, ii->second, hasNext);
    }
    o << "}";
    if (hasNext) {
      o << ",";
    }
  }
  o << "}";
}

void EmitNames(raw_ostream &o, const std::string &key, const PathStatistics::namesTy &names) {
  o << wrapString(key);
  o << ":";
  o << "{";
  for (auto i = names.begin(), e = names.end(); i != e; ++i) {
    bool hasNext = false;
    if (std::next(i) != e) {
      hasNext = true;
    }
    EmitKeyValue(o, std::to_string(i->first), i->second, hasNext);
  }
  o << "}";
}

void EmitConstraints(raw_ostream &o, const std::string &key, const PathStatistics::constraintsTy &constraints) {
  o << wrapString(key);
  o << ":";
  o << "{";
  for (auto i = constraints.begin(), e = constraints.end(); i != e; ++i) {
    bool hasNext = false;
    if (std::next(i) != e) {
      hasNext = true;
    }
    EmitKeyValue(o, std::to_string(i->first), i->second, hasNext);
  }
  o << "}";
}

void EmitEnvironment(raw_ostream &o, const std::string &key, const PathStatistics::environmentTy &env) {
  EmitKeyValue(o, key, env, false);
}

void EmitCondition(raw_ostream &o, const std::string &key, const PathStatistics::conditionTy &cond) {
  o << wrapString(key);
  o << ":";
  if (cond.empty()) {
    o << "null";
    return;
  }
  o << "{";
  EmitKeyValue(o, "variable", cond.getVariable());
  EmitKeyValue(o, "value", cond.getValue());
  EmitKeyValue(o, "operation", cond.getOperation(), false);
  o << "}";
}

void EmitPathStatistics(raw_ostream &o, const PathDiagnosticPiece &P) {
  // remove '<< ","'
  const auto &pS = P.getPathStatistics();
  // emit stmt
  EmitKeyValue(o, "stmt_class_name", pS.getStmtClassName());
  // emit names
  EmitNames(o, "names", pS.getNames());
  o << ",";
  // emit nonExprNames
  EmitNames(o, "nonExprNames", pS.getNonExprNames());
  o << ",";
  // emit bindings
  EmitBindings(o, "bindings", pS.getBindings());
  o << ",";
  // emit nonExprBindings
  EmitBindings(o, "nonExprBindings", pS.getNonExprBindings());
  o << ",";
  // emit constraints
  EmitConstraints(o, "constraints", pS.getConstraints());
  o << ",";
  EmitEnvironment(o, "environment", pS.getEnvironment());
  o << ",";
  EmitCondition(o, "condition", pS.getCondition());
  o << ",";
}

} // end anonymous namespace

SimpleStatsDiagnostics::SimpleStatsDiagnostics(AnalyzerOptions &AnalyzerOpts,
                                   const std::string& output,
                                   const LangOptions &LO,
                                   bool supportsMultipleFiles)
  : OutputFile(output),
    LangOpts(LO),
    SupportsCrossFileDiagnostics(supportsMultipleFiles) {}

void ento::createSimpleStatsDiagnosticConsumer(AnalyzerOptions &AnalyzerOpts,
                                         PathDiagnosticConsumers &C,
                                         const std::string& s,
                                         const Preprocessor &PP) {
  C.push_back(new SimpleStatsDiagnostics(AnalyzerOpts, s,
                                   PP.getLangOpts(), false));
}

void ento::createSimpleStatsMultiFileDiagnosticConsumer(AnalyzerOptions &AnalyzerOpts,
                                                  PathDiagnosticConsumers &C,
                                                  const std::string &s,
                                                  const Preprocessor &PP) {
  C.push_back(new SimpleStatsDiagnostics(AnalyzerOpts, s,
                                   PP.getLangOpts(), true));
}

static void ReportControlFlow(raw_ostream &o,
                              const PathDiagnosticControlFlowPiece &P,
                              const FIDMap &FM, const SourceManager &SM,
                              const LangOptions &LangOpts) {

  EmitKeyValue(o, "kind", "control");
  EmitPathStatistics(o, P);

  // Emit edges.
  o << wrapString("edges") << ":";
  o << "[";
  for (PathDiagnosticControlFlowPiece::const_iterator I = P.begin(),
                                                      E = P.end();
       I != E; ++I) {
    o << "{";
    // Make the ranges of the start and end point self-consistent with adjacent edges
    // by forcing to use only the beginning of the range.  This simplifies the layout
    // logic for clients.
    o << wrapString("start") << ":";

    SourceRange StartEdge(
        SM.getExpansionLoc(I->getStart().asRange().getBegin()));
    EmitRangeJson(o, SM, Lexer::getAsCharRange(StartEdge, SM, LangOpts), FM);
    o << ",";
    o << wrapString("end") << ":";
    SourceRange EndEdge(SM.getExpansionLoc(I->getEnd().asRange().getBegin()));
    EmitRangeJson(o, SM, Lexer::getAsCharRange(EndEdge, SM, LangOpts), FM);

    o << ",";

    auto RS = Lexer::getAsCharRange(StartEdge, SM, LangOpts);
    auto RE = Lexer::getAsCharRange(EndEdge, SM, LangOpts);
    o << wrapString("start_source") << ":";
    auto startStart = SM.getFileOffset(RS.getBegin());
    auto endStart = SM.getFileOffset(RS.getEnd());
    FileID FIDStart = SM.getFileID(SM.getExpansionLoc(RS.getBegin()));
    auto fileContentsStart = SM.getBufferData(FIDStart);
    o << wrapString(
        jsonEscape(fileContentsStart.slice(startStart, endStart + 1)));
    o << ",";
    o << wrapString("end_source") << ":";
    auto startEnd = SM.getFileOffset(RE.getBegin());
    auto endEnd = SM.getFileOffset(RE.getEnd());
    FileID FIDEnd = SM.getFileID(SM.getExpansionLoc(RE.getBegin()));
    auto fileContentsEnd = SM.getBufferData(FIDEnd);
    o << wrapString(
        jsonEscape(fileContentsEnd.slice(startEnd, endEnd + 1)));

    o << "}";
    if (std::next(I) != E) {
      o << ",";
    }
  }
  o << "]";
  // // Output any helper text.
  const auto &s = P.getString();
  if (!s.empty()) {
    o << ",";
    EmitKeyValue(o, "alternate", s);
  }
}

static void ReportEvent(raw_ostream &o, const PathDiagnosticPiece& P,
                        const FIDMap& FM,
                        const SourceManager &SM,
                        const LangOptions &LangOpts,
                        unsigned depth,
                        bool isKeyEvent = false) {

  EmitKeyValue(o, "kind", "event");
  EmitPathStatistics(o, P);

  // TODO WTF???
  // if (isKeyEvent) {
  //   Indent(o, indent) << "<key>key_event</key><true/>\n";
  // }

  // Output the location.
  FullSourceLoc L = P.getLocation().asLocation();

  o << wrapString("location") << ":";
  EmitLocationJson(o, SM, L, FM);

  // Output the ranges (if any).
  ArrayRef<SourceRange> Ranges = P.getRanges();

  if (!Ranges.empty()) {
    o << wrapString("ranges") << ":";
    o << "[";
    for (auto I = Ranges.begin(), E = Ranges.end(); I != E; ++I) {
      o << "{";
      o << wrapString("range") << ":";
      EmitRangeJson(
          o, SM, Lexer::getAsCharRange(SM.getExpansionRange(*I), SM, LangOpts),
          FM);
      o << ",";
      o << wrapString("source") << ":";
      auto R = Lexer::getAsCharRange(SM.getExpansionRange(*I), SM, LangOpts);
      auto start = SM.getFileOffset(R.getBegin());
      auto end = SM.getFileOffset(R.getEnd());
      FileID FID = SM.getFileID(SM.getExpansionLoc(R.getBegin()));
      auto fileContents = SM.getBufferData(FID);
      o << wrapString(jsonEscape(fileContents.slice(start, end + 1)));
      o << "}";
      if (std::next(I) != E) {
        o << ",";
      }
    }
    o << "],";
  }

  // Output the call depth.
  EmitKeyValue(o, "depth", depth);

  // Output the text.
  assert(!P.getString().empty());
  EmitKeyValue(o, "extended_message", P.getString());

  // Output the short text.
  // FIXME: Really use a short string.
  EmitKeyValue(o, "message", P.getString(), false);
}

static void ReportPiece(raw_ostream &o,
                        const PathDiagnosticPiece &P,
                        const FIDMap& FM, const SourceManager &SM,
                        const LangOptions &LangOpts,
                        unsigned depth,
                        bool includeControlFlow,
                        bool isKeyEvent = false,
                        bool hasNext = false);

static void ReportCall(raw_ostream &o,
                       const PathDiagnosticCallPiece &P,
                       const FIDMap& FM, const SourceManager &SM,
                       const LangOptions &LangOpts,
                       unsigned depth,
                       bool hasNext = true) {

  auto callEnter = P.getCallEnterEvent();
  auto callEnterWithinCaller = P.getCallEnterWithinCallerEvent();
  auto path_size = P.path.size();
  auto callExit = P.getCallExitEvent();

  if (callEnter) {
    bool hasNextCallEnter =
        hasNext or callEnterWithinCaller or callExit or path_size;
    ReportPiece(o, *callEnter, FM, SM, LangOpts, depth, true,
                P.isLastInMainSourceFile(), hasNextCallEnter);
  }


  ++depth;

  if (callEnterWithinCaller) {
    bool hasNextCallEnterWithinCaller = hasNext or callExit or path_size;
    ReportPiece(o, *callEnterWithinCaller, FM, SM, LangOpts, depth, true, false,
                hasNextCallEnterWithinCaller);
  }

  for (PathPieces::const_iterator I = P.path.begin(), E = P.path.end(); I != E;
       ++I) {
    bool hasNextSize = hasNext or std::next(I) != E or callExit;
    ReportPiece(o, **I, FM, SM, LangOpts, depth, true, false, hasNextSize);
  }

  --depth;

  if (callExit) {
    bool hasNextCallExit = hasNext;
    ReportPiece(o, *callExit, FM, SM, LangOpts, depth, true, false, hasNextCallExit);
  }
}

static void ReportMacro(raw_ostream &o,
                        const PathDiagnosticMacroPiece& P,
                        const FIDMap& FM, const SourceManager &SM,
                        const LangOptions &LangOpts,
                        unsigned depth,
                        bool hasNext) {

  for (PathPieces::const_iterator I = P.subPieces.begin(), E=P.subPieces.end();
       I!=E; ++I) {
    ReportPiece(o, **I, FM, SM, LangOpts, depth, false,  hasNext or std::next(I) != E);
  }
}

static void ReportDiag(raw_ostream &o, const PathDiagnosticPiece& P,
                       const FIDMap& FM, const SourceManager &SM,
                       const LangOptions &LangOpts,
                       bool hasNext) {
  ReportPiece(o, P, FM, SM, LangOpts, 4, 0, true, hasNext);
}

static void ReportPiece(raw_ostream &o,
                        const PathDiagnosticPiece &P,
                        const FIDMap& FM, const SourceManager &SM,
                        const LangOptions &LangOpts,
                        unsigned depth,
                        bool includeControlFlow,
                        bool isKeyEvent,
                        bool hasNext) {
  switch (P.getKind()) {
    case PathDiagnosticPiece::ControlFlow:
      if (includeControlFlow) {
        o << "{";
        ReportControlFlow(o, cast<PathDiagnosticControlFlowPiece>(P), FM, SM,
                          LangOpts);
        o << "}";
        if (hasNext) {
          o << ",";
        }
      }
      break;
    case PathDiagnosticPiece::Call:
      ReportCall(o, cast<PathDiagnosticCallPiece>(P), FM, SM, LangOpts,
                 depth, hasNext);
      break;
    case PathDiagnosticPiece::Event:
      o << "{";
      ReportEvent(o, cast<PathDiagnosticSpotPiece>(P), FM, SM, LangOpts,
                  depth, isKeyEvent);
      o << "}";
      if (hasNext) {
        o << ",";
      }
      break;
    case PathDiagnosticPiece::Macro:
      ReportMacro(o, cast<PathDiagnosticMacroPiece>(P), FM, SM, LangOpts,
                  depth, hasNext);
      break;
    case PathDiagnosticPiece::Note:
      o << "{";
      o << "}";
      if (hasNext) {
        o << ",";
      }
      // FIXME: Extend the plist format to support those.
      break;
  }
}

void SimpleStatsDiagnostics::FlushDiagnosticsImpl(
                                    std::vector<const PathDiagnostic *> &Diags,
                                    FilesMade *filesMade) {
  // Build up a set of FIDs that we use by scanning the locations and
  // ranges of the diagnostics.
  FIDMap FM;
  SmallVector<FileID, 10> Fids;
  const SourceManager* SM = nullptr;

  if (!Diags.empty())
    SM = &Diags.front()->path.front()->getLocation().getManager();

  auto AddPieceFID = [&FM, &Fids, SM](const PathDiagnosticPiece &Piece) {
    AddFID(FM, Fids, *SM, Piece.getLocation().asLocation());
    ArrayRef<SourceRange> Ranges = Piece.getRanges();
    for (const SourceRange &Range : Ranges) {
      AddFID(FM, Fids, *SM, Range.getBegin());
      AddFID(FM, Fids, *SM, Range.getEnd());
    }
  };

  for (const PathDiagnostic *D : Diags) {

    SmallVector<const PathPieces *, 5> WorkList;
    WorkList.push_back(&D->path);

    while (!WorkList.empty()) {
      const PathPieces &Path = *WorkList.pop_back_val();

      for (const auto &Iter : Path) {
        const PathDiagnosticPiece &Piece = *Iter;
        AddPieceFID(Piece);

        if (const PathDiagnosticCallPiece *Call =
                dyn_cast<PathDiagnosticCallPiece>(&Piece)) {
          if (auto CallEnterWithin = Call->getCallEnterWithinCallerEvent())
            AddPieceFID(*CallEnterWithin);

          if (auto CallEnterEvent = Call->getCallEnterEvent())
            AddPieceFID(*CallEnterEvent);

          WorkList.push_back(&Call->path);
        } else if (const PathDiagnosticMacroPiece *Macro =
                       dyn_cast<PathDiagnosticMacroPiece>(&Piece)) {
          WorkList.push_back(&Macro->subPieces);
        }
      }
    }
  }

  // Open the file.
  std::error_code EC;
  llvm::raw_fd_ostream o(OutputFile, EC, llvm::sys::fs::F_Text);
  if (EC) {
    llvm::errs() << "warning: could not create file: " << EC.message() << '\n';
    return;
  }
  
  // Start json

  o << "{";

  EmitKeyValue(o, "clang_version", getClangFullVersion());

  o << "\"files\" : [";

  for (auto I = Fids.begin(), E = Fids.end(); I != E; ++I) {
    o << wrapString(SM->getFileEntryForID(*I)->getName());
    if (std::next(I) != E) {
      o << ",";
    }
  }

  o << "],";

  o << wrapString("diagnostics") << ":" <<  "[";


  for (std::vector<const PathDiagnostic*>::iterator DI=Diags.begin(),
       DE = Diags.end(); DI!=DE; ++DI) {

    o << "{";

    o << "\"path\" : ";

    const PathDiagnostic *D = *DI;

    o << "[";

    for (PathPieces::const_iterator I = D->path.begin(), E = D->path.end();
         I != E; ++I) {
      ReportDiag(o, **I, FM, *SM, LangOpts, std::next(I) != E);
    }

    o << "],";

    // Output the bug type and bug category.
    EmitKeyValue(o, "description", D->getShortDescription());
    EmitKeyValue(o, "category", D->getCategory());
    EmitKeyValue(o, "type", D->getBugType());
    EmitKeyValue(o, "check_name", D->getCheckName());

    // o << "   <!-- This hash is experimental and going to change! -->\n";
    PathDiagnosticLocation UPDLoc = D->getUniqueingLoc();
    FullSourceLoc L(SM->getExpansionLoc(UPDLoc.isValid()
                                            ? UPDLoc.asLocation()
                                            : D->getLocation().asLocation()),
                    *SM);
    const Decl *DeclWithIssue = D->getDeclWithIssue();
    EmitKeyValue(o, "issue_hash_content_of_line_in_context",
                 GetIssueHash(*SM, L, D->getCheckName(), D->getBugType(),
                              DeclWithIssue, LangOpts).c_str());

    // Output information about the semantic context where
    // the issue occurred.
    if (const Decl *DeclWithIssue = D->getDeclWithIssue()) {
      // FIXME: handle blocks, which have no name.
      if (const NamedDecl *ND = dyn_cast<NamedDecl>(DeclWithIssue)) {
        StringRef declKind;
        switch (ND->getKind()) {
          case Decl::CXXRecord:
            declKind = "C++ class";
            break;
          case Decl::CXXMethod:
            declKind = "C++ method";
            break;
          case Decl::ObjCMethod:
            declKind = "Objective-C method";
            break;
          case Decl::Function:
            declKind = "function";
            break;
          default:
            break;
        }
        if (!declKind.empty()) {
          const std::string &declName = ND->getDeclName().getAsString();
          EmitKeyValue(o, "issue_context_kind", declKind);
          EmitKeyValue(o, "issue_context", declName);
        }

        // Output the bug hash for issue unique-ing. Currently, it's just an
        // offset from the beginning of the function.
        if (const Stmt *Body = DeclWithIssue->getBody()) {

          // If the bug uniqueing location exists, use it for the hash.
          // For example, this ensures that two leaks reported on the same line
          // will have different issue_hashes and that the hash will identify
          // the leak location even after code is added between the allocation
          // site and the end of scope (leak report location).
          if (UPDLoc.isValid()) {
            FullSourceLoc UFunL(SM->getExpansionLoc(
              D->getUniqueingDecl()->getBody()->getLocStart()), *SM);
            EmitKeyValue(o, "issue_hash_function_offset",
                         L.getExpansionLineNumber() -
                             UFunL.getExpansionLineNumber());
          // Otherwise, use the location on which the bug is reported.
          } else {
            FullSourceLoc FunL(SM->getExpansionLoc(Body->getLocStart()), *SM);
            EmitKeyValue(o, "issue_hash_function_offset",
                         L.getExpansionLineNumber() -
                             FunL.getExpansionLineNumber());
          }
        }
      }
    }

    // Output the location of the bug.
    o << wrapString("location") << ":";
    EmitLocationJson(o, *SM, D->getLocation().asLocation(), FM, false);

    // TODO WTF???
    // Output the diagnostic to the sub-diagnostic client, if any.
    // if (!filesMade->empty()) {
    //   StringRef lastName;
    //   PDFileEntry::ConsumerFiles *files = filesMade->getFiles(*D);
    //   if (files) {
    //     for (PDFileEntry::ConsumerFiles::const_iterator CI =
    //     files->begin(),
    //             CE = files->end(); CI != CE; ++CI) {
    //       StringRef newName = CI->first;
    //       if (newName != lastName) {
    //         if (!lastName.empty()) {
    //           o << "  </array>\n";
    //         }
    //         lastName = newName;
    //         o <<  "  <key>" << lastName << "_files</key>\n";
    //         o << "  <array>\n";
    //       }
    //       o << "   <string>" << CI->second << "</string>\n";
    //     }
    //     o << "  </array>\n";
    //   }
    // }

    // Close up the entry.
    o << "}";
    if (std::next(DI) != DE) {
      o << ",";
    }
  }

  o << "]";

  // Finish.
  o << "}";
}
