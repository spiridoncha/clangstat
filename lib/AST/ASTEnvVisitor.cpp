#include "clang/AST/ASTEnvVisitor.h"

#include "llvm/Support/raw_ostream.h"
#include "clang/AST/Stmt.h"

using namespace llvm;
using namespace clang;

std::string
ASTEnvVisitor::getNodeName(const ast_type_traits::DynTypedNode &node) const {
  if (const Stmt *S = node.get<Stmt>()) {
    return S->getStmtClassName();
  }
  if (const Decl *D = node.get<Decl>()) {
    return D->getDeclKindName();
  }
  if (const Type *T = node.get<Type>()) {
    return T->getTypeClassName();
  }
  return "None";
}

// Maybe need to do second optionally arg for 'print' purpose
auto ASTEnvVisitor::getEnv(const Stmt *S) const -> EnvContainer {
  EnvContainer ngram;
  if (!S) {
    return ngram;
  }
  if (preMap.find(S) == preMap.end()) {
    return ngram;
  }
  EnvContainer::value_type startGram(S->getStmtClassName());
  ngram.push_back(startGram);
  decltype(counter) child_begin = preMap.find(S)->second;
  decltype(counter) child_end = postMap.find(S)->second;
  radiusType need_left = envRadius;
  radiusType need_right = envRadius;
  // front
  for (decltype(counter) j = child_begin - 1; need_left > 0 and j > 0;
       --j) {
    if (data.find(j) == data.end()) {
      continue;
    }
    ngram.push_front(getNodeName((data.find(j))->second));
    --need_left;
  }
  while (need_left-- > 0) {
    ngram.push_front("None");
  }
  // back
  for (decltype(counter) j = child_end + 1;
       need_right > 0 and j <= preVisitLimit; ++j) {
    if (data.find(j) == data.end()) {
      continue;
    }
    ngram.push_back(getNodeName((data.find(j))->second));
    --need_right;
  }
  while (need_right-- > 0) {
    ngram.push_back("None");
  }
  return ngram;
}

void ASTEnvVisitor::dump() const {
  llvm::errs() << data.size() << "\n";
  for (decltype(counter) i = 0; i != counter; ++i) {
    auto it = data.find(i);
    if (it == data.end()) {
      continue;
    }
    ast_type_traits::DynTypedNode node = it->second;
    if (const Stmt *S = node.get<Stmt>()) {
      llvm::errs() << S->getStmtClassName() << " " << i << " "
                   << (postMap.find(S)->second) << "\n";
    }
    if (const Decl *D = node.get<Decl>()) {
      llvm::errs() << D->getDeclKindName() << " " << i << "\n";
    }
    if (const Type *T = node.get<Type>()) {
      llvm::errs() << T->getTypeClassName() << " " << i << "\n";
    }
  }
}

void ASTEnvVisitor::print(raw_ostream &o) const {
  for (auto i = preMap.begin(), e = preMap.end(); i != e; ++i) {
    EnvContainer env = getEnv(i->first);
    for (auto ii = env.begin(), ie = env.end(); ii != ie; ++ii) {
      o << *ii;
      if (std::next(ii) != ie) {
        o << " ";
      }
    }
    o << "\n";
  }
}

