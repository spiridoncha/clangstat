#include "clang/AST/ASTConsumer.h"
#include "clang/AST/ASTEnvVisitor.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/FrontendAction.h"
#include "clang/Tooling/CommonOptionsParser.h"
#include "clang/Tooling/Tooling.h"
#include "llvm/Support/CommandLine.h"
#include "llvm/Support/raw_ostream.h"

#include "clang/AST/ASTTypeTraits.h"

#include <fstream>
#include <unordered_map>

using namespace llvm;
using namespace clang;
using namespace clang::tooling;

static llvm::cl::OptionCategory GetAstEnvStatCategory("getAstEnvStat options");

static cl::extrahelp CommonHelp(CommonOptionsParser::HelpMessage);

static cl::extrahelp MoreHelp("\nMore help text...");

static cl::opt<std::string> fileName("o",
                                     cl::desc("Name of output file"),
                                     cl::value_desc("absolute_filepath"),
                                     cl::cat(GetAstEnvStatCategory),
                                     cl::Required);

class FullASTConsumer : public ASTConsumer {
public:
  FullASTConsumer() : visitor() {}

  void HandleTranslationUnit(ASTContext &Context) override {
    visitor.TraverseDecl(Context.getTranslationUnitDecl());
    std::error_code EC;
    // global fileName
    raw_fd_ostream os(fileName, EC, sys::fs::F_None | sys::fs::F_Append);
    visitor.print(os);
  }

private:
  ASTEnvVisitor visitor;
};

class ASTProcessorAction : public ASTFrontendAction {
public:
  ASTProcessorAction() {}

  std::unique_ptr<ASTConsumer> newASTConsumer() {
    return llvm::make_unique<FullASTConsumer>();
  }

protected:
  std::unique_ptr<ASTConsumer> CreateASTConsumer(CompilerInstance &CI,
                                                 StringRef InFile) override;
};

std::unique_ptr<ASTConsumer>
ASTProcessorAction::CreateASTConsumer(CompilerInstance &CI, StringRef InFile) {
  return newASTConsumer();
}

class RunnerFromCompilationsDatabase {
public:
  RunnerFromCompilationsDatabase(CommonOptionsParser &parser)
      : optionsParser(parser) {}
  int run() {
    auto files = optionsParser.getCompilations().getAllFiles();
    ASTProcessorAction consumerFactory;
    ClangTool Tool(optionsParser.getCompilations(), files);
    auto ret = Tool.run(
        newFrontendActionFactory<ASTProcessorAction>(&consumerFactory).get());
    return ret;
  }

private:
  CommonOptionsParser &optionsParser;
};

int main(int argc, const char **argv) {
  CommonOptionsParser OptionsParser(argc, argv, GetAstEnvStatCategory);
  RunnerFromCompilationsDatabase runner(OptionsParser);
  return runner.run();
}
