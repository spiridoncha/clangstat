set(LLVM_LINK_COMPONENTS support)
# set(LLVM_USED_LIBS clangTooling clangBasic clangAST)

# include_directories(include)
# add_subdirectory(lib)

add_clang_tool(clang-ast-environment-stat
  getAstEnvStat.cpp
)

# add_clang_executable(clang-ast-environment-stat
#   getAstEnvStat.cpp
# )

target_link_libraries(clang-ast-environment-stat
  clangTooling
  clangBasic
  clangASTMatchers
)

install(PROGRAMS clang-ast-environment-stat
  DESTINATION bin
  COMPONENT clang-ast-environment-stat
)
