//===--- PathStatistics.h - Path-Specific Statictics Collector --*- C++ -*-===//
//
//                     The LLVM Compiler Infrastructure
//
// This file is distributed under the University of Illinois Open Source
// License. See LICENSE.TXT for details.
//
//===----------------------------------------------------------------------===//
//
//  This file defines the PathStatistics interfaces.
//
//===----------------------------------------------------------------------===//

#ifndef LLVM_CLANG_STATICANALYZER_CORE_BUGREPORTER_PATHSTATISTICS_H
#define LLVM_CLANG_STATICANALYZER_CORE_BUGREPORTER_PATHSTATISTICS_H

#include "clang/StaticAnalyzer/Core/PathSensitive/Store.h"
#include "clang/StaticAnalyzer/Core/PathSensitive/ConstraintManager.h"
#include <memory>
#include <unordered_map>
#include <utility>

namespace llvm {
  class raw_ostream;
}

namespace clang {
  class ASTContext;
  class Stmt;
  class ASTEnvVisitor;
}

namespace clang {

namespace ento {

class ProgramState;
class StoreManager;

class PathStatistics {
private:
  class PathStatisticsImpl;
  std::unique_ptr<PathStatisticsImpl> pImpl;

public:
  PathStatistics();
  PathStatistics(const PathStatistics &) = delete;
  PathStatistics(PathStatistics &&) = delete;
  PathStatistics &operator=(const PathStatistics &) = delete;
  PathStatistics &operator=(PathStatistics &&) = delete;
  ~PathStatistics();

  class Condition {
  private:
    std::string variable;
    std::string value;
    std::string operation;

  public:
    Condition(const std::string &var = "", const std::string &val = "",
              const std::string &op = "")
        : variable(var), value(val), operation(op) {}

    std::string getCond() const {
      return variable + ' ' + operation + ' ' + value;
    }

    bool empty() const { return operation == ""; }

    const std::string &getVariable() const { return variable; }
    const std::string &getValue() const { return value; }
    const std::string &getOperation() const { return operation; }
  };

  typedef std::unordered_map<
      decltype(std::declval<llvm::FoldingSetNodeID>().ComputeHash()),
      std::unordered_map<std::string, bool>>
      bindingsTy;

  typedef std::unordered_map<
      decltype(std::declval<llvm::FoldingSetNodeID>().ComputeHash()),
      std::string>
      namesTy;

  typedef ConstraintManager::constraintsTy constraintsTy;
  typedef std::string environmentTy;
  typedef Condition conditionTy;

  void addPartOfPathStatistics(const Stmt *);
  void addPartOfPathStatistics(const std::string &, const std::string &,
                               const std::string &);
  void addPartOfPathStatistics(ASTContext &, const Stmt *, const ASTEnvVisitor &);
  void addPartOfPathStatistics(StoreManager &, const ProgramState &);

  const std::string &getStmtClassName() const;
  void setStmtClassName(const std::string &);

  const bindingsTy &getBindings() const;
  const bindingsTy &getNonExprBindings() const;
  const namesTy &getNames() const;
  const namesTy &getNonExprNames() const;
  const constraintsTy &getConstraints() const;
  const environmentTy &getEnvironment() const;
  const conditionTy &getCondition() const;

  void dump() const;
  void dump(llvm::raw_ostream &) const;
};

} // end ento namespace

} // end clang namespace

#endif
