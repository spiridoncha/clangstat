#ifndef LLVM_CLANG_AST_ASTENVVISITOR_H
#define LLVM_CLANG_AST_ASTENVVISITOR_H

// need to <>
#include "llvm/ADT/DenseMap.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include <unordered_map>
#include <deque>
#include <string>

namespace clang {
  class Stmt;
  class raw_ostream;
  namespace ast_type_traits {
    class DynTypedNode;
  }
}

namespace clang {

class ASTEnvVisitor : public clang::RecursiveASTVisitor<ASTEnvVisitor> {
public:
  typedef unsigned radiusType;
  ASTEnvVisitor()
      : counter(0), preVisitLimit(0) {}

private:
  int counter = 0;
  decltype(counter) preVisitLimit = 0;
  radiusType envRadius = 3;

  std::unordered_map<decltype(counter), clang::ast_type_traits::DynTypedNode> data;
  llvm::DenseMap<Stmt *, decltype(counter)> postMap;
  llvm::DenseMap<Stmt *, decltype(counter)> preMap;

  std::string getNodeName(const clang::ast_type_traits::DynTypedNode &) const;

  void updateCounter(bool both = true) {
    ++counter;
    if (both) {
      preVisitLimit = counter;
    }
  }


public:
  typedef std::deque<std::string> EnvContainer;
  EnvContainer getEnv(const Stmt *) const;

  bool VisitStmt(Stmt *S) {
    updateCounter();
    preMap.insert({S, counter});
    data.insert({counter, ast_type_traits::DynTypedNode::create(*S)});
    return true;
  }

  bool VisitDecl(Decl *D) {
    updateCounter();
    data.insert({counter, ast_type_traits::DynTypedNode::create(*D)});
    return true;
  }

  bool VisitType(Type *T) {
    updateCounter();
    data.insert({counter, clang::ast_type_traits::DynTypedNode::create(*T)});
    return true;
  }

  bool dataTraverseStmtPost(Stmt *S) {
    updateCounter(false);
    postMap.insert({S, counter});
    return true;
  }

  void print(raw_ostream &) const;
  void dump() const;
};

} // end clang namespace

#endif
